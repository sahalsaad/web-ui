declare let require: any;

const canyaAbi = require('assets/abi/canyaABI.json');
const daoAbi = require('assets/abi/daoABI.json');

export {
    canyaAbi,
    daoAbi
};
